#include "parser.h"
#include <vector>
#include <string>
#include <ext/vstring.h>

typedef std::vector<__gnu_cxx::__sso_string> strings;

// concatenate two lists
strings concat(strings a, strings b) { 
  a.insert(a.end(), b.begin(), b.end());
  return a;
}

// combination of all elements. [a,b] [1,2] -> [a1,a2,b1,b2]
strings combine(strings a, strings b) { 
  strings res;
  for (auto i : a)
    for (auto j : b)
      res.push_back(i+j);
  return res;
}

strings make_digit19() { 
  // testing all nine digits uses a LOT of memory
  return strings{ "1", "2", "3", "5", "7" };
}

strings make_digit() { 
  return concat(strings{"0"}, make_digit19());
}

strings make_digits() { 
  auto d = make_digit();
  return concat(d, combine(d, d));
}

strings make_e() {
  return combine(strings{ "e", "E" }, strings{ "", "+", "-"});
}

strings make_exp() {
  return combine(make_e(), make_digits());
}

strings make_frac() {
  strings dot{ "." };
  return combine(dot, make_digits());
}

strings make_int() {
  strings sign{ "", "-" };
  auto digits = concat(make_digit(), combine(make_digit19(), make_digit/*s*/()));
  return combine(sign, digits);
}

strings make_number() {
  auto i = make_int();
  auto f = make_frac();
  auto e = make_exp();
  auto i_f = combine(i, f);
  auto i_e = combine(i_f, e);
  auto i_f_e = combine(i_f, e);
  return concat(i, concat(i_f, concat(i_e, i_f_e)));
}

int main()
{
  for (auto s : make_number())
  {
    assert( json::parse_number(s.begin(), s.end()) == s.end() );
    s += "  ";
    assert( json::parse_number(s.begin(), s.end()) == s.end() - 2 );
  }
}

