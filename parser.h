#ifndef JSON_PARSER_H
#define JSON_PARSER_H

// Copyright Jonathan Wakely 2011
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include "types.h"
#include <string>
#include <iterator>
#include <forward_list>
#include <cassert>

namespace json
{
  template<typename FwdIter, typename Sequence>
    FwdIter parse(FwdIter begin, FwdIter end, Sequence& seq);

  constexpr char to_char(char c) { return c; }

  constexpr char to_escape(char c)
  {
    return c == 'b' ? '\b'
         : c == 'f' ? '\f'
         : c == 'n' ? '\n'
         : c == 'r' ? '\r'
         : c == 't' ? '\t'
         : '\0';
  }

  constexpr char to_char(char32_t c)
  {
    return c == U'"' ? '"'
         : c == U'\\' ? '\\'
         : c == U'/' ? '/'
         : c == U'b' ? 'b'
         : c == U'f' ? 'f'
         : c == U'n' ? 'n'
         : c == U'r' ? 'r'
         : c == U't' ? 't'
         : c == U'u' ? 'u'
         : c == U'{' ? '{'
         : c == U'}' ? '}'
         : c == U'[' ? '['
         : c == U']' ? ']'
         : c == U':' ? ':'
         : c == U',' ? ','
         : '\0';
  }

  constexpr char32_t to_escape(char32_t c)
  {
    return c == U'b' ? U'\b'
         : c == U'f' ? U'\f'
         : c == U'n' ? U'\n'
         : c == U'r' ? U'\r'
         : c == U't' ? U'\t'
         : U'\0';
  }

  template<typename FwdIter>
    class parsed_range
    {
      struct subrange
      {
        // Constructs an invalid value
        subrange() : c(), begin(), end()
        { }

        explicit
        subrange(char32_t c) : c(c), begin(), end()
        { assert(c != 0); }

        explicit
        subrange(FwdIter b, FwdIter e) : c(0), begin(b), end(e)
        { assert(b != e); }

        bool is_range() const { return begin != end; }

        bool valid() const { return c != 0 || is_range(); }

        char32_t& get() { return c ? c : (c = *begin); }

        char32_t c;
        FwdIter begin, end;
      };
      typedef std::forward_list<subrange> list_type;
      typedef typename list_type::const_iterator list_iterator;

    public:

      struct iterator
      {
        typedef char32_t value_type;
        typedef std::ptrdiff_t difference_type;
        typedef std::forward_iterator_tag iterator_category;
        typedef const value_type& reference;
        typedef const value_type* pointer;

        explicit
        iterator(list_iterator i) : list_iter(i), value() { }

        iterator() = default;
        iterator(const iterator&) = default;
        iterator(iterator&&) = default;

        ~iterator() = default;

        iterator& operator=(const iterator&) = default;
        iterator& operator=(iterator&&) = default;

        // invariant: if *this is dereferencable then ... ?
        reference
        operator*() const
        {
          if (!value.valid())
            value = *list_iter;
          return value.get();
        }

        pointer operator->() const { return &**this; }

        iterator
        operator++()
        {
          // invariant: if *this is incrementable then ... ?
          if (list_iter->is_range())
          {
            if (!value.valid())
              value = *list_iter;
            value.c = 0;
            if (++value.begin != value.end)
              return *this;
          }
          ++list_iter;
          invalidate();
          return *this;
        }

        iterator
        operator++(int)
        {
          iterator tmp(*this);
          ++*this;
          return tmp;
        }

        bool
        operator==(iterator const& that) const
        {
          if (list_iter == that.list_iter)
          {
            if (value.valid() && that.value.valid())
              return value.begin == that.value.begin;
            if (value.valid())
              return value.begin == that.list_iter->begin;
            if (that.value.valid())
              return list_iter->begin == that.value.begin;
            return true;
          }
          return false;
        }

        bool operator!=(iterator const& that) const { return !(*this == that); }

      private:
        void invalidate() { value = subrange(); }

        void bump()
        {
          assert( value.begin != value.end );
          value.c = 0;
          if (++value.begin == value.end)
          {
            ++list_iter;
            invalidate();
          }
        }

        list_iterator list_iter;
        mutable subrange value;
        friend class parsed_range;
      };
      typedef iterator const_iterator;

      parsed_range() : last(list.before_begin())
      { }

      parsed_range(FwdIter b, FwdIter e)
      : list{subrange{b, e}}, last(list.begin())
      { }

      parsed_range(iterator b, iterator e)
      : list{copy_list(b, e)}, last(list.before_begin())
      {
        for (list_iterator i = last; ++i != list.end(); last = i)
        { }
      }

      // Requires c != 0
      void
      emplace_back(char32_t c)
      { last = list.emplace_after(last, c); }

      // Requires b != e
      void
      emplace_back(FwdIter b, FwdIter e)
      { last = list.emplace_after(last, b, e); }

      const_iterator begin() const { return iterator{list.begin()}; }
      const_iterator end() const { return iterator{list.end()}; }
      const_iterator cbegin() const { return begin(); }
      const_iterator cend() const { return end(); }

      bool empty() const { return list.empty(); }

    private:

      static list_type copy_list(iterator b, iterator e)
      {
        list_type l;
        if (!b.value.valid() && !e.value.valid())
          l = list_type{b.list_iter, e.list_iter};
        else
          l = list_type{b, e};
        return l;
      }

      list_type list;
      list_iterator last;
    };

  // Parse a string, stripping quotes and expanding special characters,
  // and assign the result to seq.
  // Return an iterator to the character after the closing quote character.
  template<typename FwdIter, typename Seq>
    FwdIter
    parse_string(FwdIter begin, FwdIter end, Seq& seq)
    {
      if (begin == end || to_char(*begin) != '"')
        throw parse_error("Expected '\"' at start of string");
      FwdIter curr = ++begin;
      parsed_range<FwdIter> r;
      while (curr != end)
      {
        switch (to_char(*curr))
        {
        case '"':
          if (r.empty())
            seq = Seq{begin, curr};
          else
          {
            if (begin != curr)
              r.emplace_back(begin, curr);
            seq = Seq{r.begin(), r.end()};
          }
          return ++curr;
        case '\\':
          if (begin != curr)
            r.emplace_back(begin, curr);
          switch (to_char(*++curr))
          {
          case '"':
          case '\\':
          case '/':
            r.emplace_back(*curr++);
            break;
          case 'b':
          case 'f':
          case 'n':
          case 'r':
          case 't':
            r.emplace_back(to_escape(*curr++));
            break;
          case 'u':
            {
              std::string buf;
              buf.reserve(4);
              while (++curr != end && buf.length() < 4)
                buf += to_char(*curr);
              try
              {
                char* e;
                char32_t ucn = std::strtoul(buf.c_str(), &e, 16);
                if (e < buf.c_str()+4)
                  throw parse_error("Invalid UCN");
                r.emplace_back(ucn);
              }
              catch (std::invalid_argument const&)
              {
                throw parse_error("Invalid UCN");
              }
            }
            break;
          default:
            std::string s("Invalid escape sequence: '\\");
            throw parse_error(s + to_char(*curr) + '\'');
          }
          begin = curr;
          break;
        default:
          ++curr;
          break;
        };
      }
      throw parse_error("Expected '\"' at end of string");
    }

  // Parse a number, return an iterator to the character after the number.
  template<typename FwdIter>
    FwdIter
    parse_number(FwdIter begin, FwdIter end)
    {
      auto consume_digits = [&] {
        while (++begin != end && isdigit(to_char(*begin)))
        { }
      };

      if (begin == end)
        throw parse_error("Expected sign or digit at start of number");

      // int
      char c = to_char(*begin);
      if (c == '-')
      {
        if (++begin == end)
          throw parse_error("Expected digit after sign of number");
        c = to_char(*begin);
      }
      if (c == '0')
        ++begin;
      else if (isdigit(c))
        consume_digits();
      else
        throw parse_error("Expected non-zero digit at start of number");
      if (begin == end)
        return end;

      // frac
      c = to_char(*begin);
      if (c == '.')
      {
        consume_digits();
        if (begin == end)
          return end;
        c = to_char(*begin);
      }

      // exp
      if (c == 'e' || c == 'E')
      {
        if (++begin == end)
          throw parse_error("Expected sign or digit at start of exponent");
        c = to_char(*begin);
        if (c == '-' || c == '+')
        {
          if (++begin == end)
            throw parse_error("Expected digit after sign of exponent");
          c = to_char(*begin);
        }
        if (!isdigit(c))
          throw parse_error("Expected digit at start of exponent");
        consume_digits();
      }
      return begin;
    }

  template<typename FwdIter, typename Pair>
    FwdIter
    parse_number(FwdIter begin, FwdIter end, Pair& range)
    {
      auto first = begin;
      auto last = parse_number(begin, end);
      range = Pair{first, last};
      return last;
    }

} // namespace json

#endif
