CXXFLAGS = -std=c++0x -Wfatal-errors -g

all: check

check: test/test_num
	@for t in $^ ; do ./$$t ; done

test/%: test/%.cc parser.h types.h
	$(CXX) -I. $(CXXFLAGS) $(LDFLAGS) -o $@ $<
