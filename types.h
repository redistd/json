#ifndef JSON_TYPES_H
#define JSON_TYPES_H

// Copyright Jonathan Wakely 2011
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt or copy at
// http://www.boost.org/LICENSE_1_0.txt)

#include <boost/variant/variant.hpp>
#include <boost/variant/recursive_wrapper.hpp>
#include <string>
#include <vector>
#include <unordered_map>
#include <stdexcept>

namespace json
{
  class value;
  typedef std::string string;
  typedef double number;
  typedef bool* literal;
  typedef std::vector<value> array;
  typedef std::unordered_map<string, value> object;

  class value
  {
    typedef boost::variant< string, number, literal,
                            boost::recursive_wrapper<array>,
                            boost::recursive_wrapper<object>
                          > variant_type;
    variant_type val;

  public:

    explicit operator string() const;
    explicit operator number() const;
    explicit operator literal() const;
  };

  typedef object::value_type member;

  struct parse_error : std::runtime_error
  {
    // TODO use error_code
    explicit parse_error(std::string const& s) : runtime_error(s) { }
  };

}  // namespace json

#endif

